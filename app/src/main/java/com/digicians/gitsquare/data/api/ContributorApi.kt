package com.digicians.gitsquare.data.api

import com.digicians.gitsquare.model.Contributor
import io.reactivex.Single
import retrofit2.http.GET


interface ContributorApi {

    @GET("")
    fun getContributors(): Single<List<Contributor>>
}