package com.digicians.gitsquare.data.repository

import com.digicians.gitsquare.model.Contributor
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContributorRepository @Inject constructor() {

    fun getContributors(): Single<List<Contributor>> {
        return Single.just(listOf())
    }

}