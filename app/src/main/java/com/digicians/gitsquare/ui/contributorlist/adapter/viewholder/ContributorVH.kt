package com.digicians.gitsquare.ui.contributorlist.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.digicians.gitsquare.databinding.ItemContributorBinding
import com.digicians.gitsquare.model.Contributor

class ContributorVH(private val binding: ItemContributorBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(contributor: Contributor) {
        binding.contributor = contributor
        binding.executePendingBindings()
    }

}