package com.digicians.gitsquare.ui.contributorlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.digicians.gitsquare.databinding.ItemContributorBinding
import com.digicians.gitsquare.model.Contributor
import com.digicians.gitsquare.ui.contributorlist.adapter.viewholder.ContributorVH

class ContributorAdapter : RecyclerView.Adapter<ContributorVH>() {

    private var contributors: List<Contributor>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContributorVH {
        return ItemContributorBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ).run {
            ContributorVH(this)
        }
    }

    override fun getItemCount() = contributors?.size ?: 0

    override fun onBindViewHolder(holder: ContributorVH, position: Int) {
        holder.bind(contributors!![position])
    }

    fun updateContributor(contributors: List<Contributor>) {
        this.contributors = contributors
        notifyDataSetChanged()
    }
}
