package com.digicians.gitsquare.ui.contributorlist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.digicians.gitsquare.R
import com.digicians.gitsquare.databinding.ActivityContributorListBinding
import com.digicians.gitsquare.ui.contributorlist.adapter.ContributorAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class ContributorListActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ContributorListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        val binding: ActivityContributorListBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_contributor_list
        )

        val activity = this

        with(binding) {
            viewModel = activity.viewModel

            contributorRV.adapter = ContributorAdapter()
        }

        with(viewModel) {
            loadContributors()
        }
    }

}