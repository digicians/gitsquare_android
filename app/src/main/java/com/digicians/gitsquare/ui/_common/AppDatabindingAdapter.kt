package com.digicians.gitsquare.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.digicians.gitsquare.model.Contributor
import com.digicians.gitsquare.ui.contributorlist.adapter.ContributorAdapter

object AppDatabindingAdapter {

    @BindingAdapter("contributors")
    @JvmStatic
    fun bindContributors(recyclerView: RecyclerView, contributors: List<Contributor>?) {
        contributors?.let {
            (recyclerView.adapter as? ContributorAdapter)?.updateContributor(it)
        }
    }

}
