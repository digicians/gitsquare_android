package com.digicians.gitsquare.ui.contributorlist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.digicians.gitsquare.data.repository.ContributorRepository
import com.digicians.gitsquare.model.Contributor
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

private val TAG = ContributorListViewModel::class.java.simpleName

class ContributorListViewModel @Inject constructor(
    private val contributorRepository: ContributorRepository
) : ViewModel() {

    val contributors = MutableLiveData<List<Contributor>>()

    private val disposables = CompositeDisposable()

    fun loadContributors() {
        contributorRepository.getContributors()
            .subscribe(
                {
                    Log.d(TAG, "Contributor list loaded")
                    contributors.value = it
                },
                {
                    Log.e(TAG, "Could not get contributor list", it)
                }
            )
            .also {
                disposables.add(it)
            }
    }

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }
}