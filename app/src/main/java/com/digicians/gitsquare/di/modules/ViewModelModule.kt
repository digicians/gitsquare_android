package com.digicians.gitsquare.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.digicians.gitsquare.ui.contributorlist.ContributorListViewModel
import com.digicians.gitsquare.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ContributorListViewModel::class)
    abstract fun bindContributorListViewModel(contributorListViewModel: ContributorListViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

}