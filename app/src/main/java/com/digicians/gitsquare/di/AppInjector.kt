package com.digicians.gitsquare.di

import com.digicians.gitsquare.App

object AppInjector {

    fun init(app: App) {
        DaggerAppComponent
            .builder()
            .application(app)
            .build()
            .inject(app)
    }
}