package com.digicians.gitsquare.di.modules

import com.digicians.gitsquare.ui.contributorlist.ContributorListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ContributorListActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeContributorListActivity(): ContributorListActivity
}