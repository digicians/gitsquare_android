package com.digicians.gitsquare.di

import android.app.Application
import com.digicians.gitsquare.App
import com.digicians.gitsquare.di.modules.ContributorListActivityModule
import com.digicians.gitsquare.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,

        ViewModelModule::class,

        ContributorListActivityModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
