package com.digicians.gitsquare.model

import com.google.gson.annotations.SerializedName

data class Contributor(
    val login: String,
    @SerializedName("avatar_url") val avatarUrl: String,
    @SerializedName("repos_url") val reposUrl: String,
    val contributions: Int
) : Comparable<Contributor> {

    override fun compareTo(other: Contributor) = contributions.compareTo(other.contributions)
}